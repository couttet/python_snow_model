# Minimal snow model 

## Synopsis
This project is a program containning a very simplified 1D snow model. It solves the coupled set of temperature and water vapor density PDEs using the Finite Difference Method.

## Usage
You can run the program as following: `python main.py number_of_nodes`
For example: `python main.py 21` will run the program and solve the equations over 21 nodes

In the main.py you can specify the following values:
* Numerical parameters: time step, end time
* Physical parameters: snow depth, snow density
* Boundary and initial conditions for both temperature and vapor density
* Validation parameters: validation (True or False), validation depth
* Visualization: do print (True or False) 


## Motivation
This project has been done in the frame of the Scientic Programming for Engineer class (2016-2017), and has the goal to:
* understand how complex can rapidly get the modelling of snowpacks 
* how can be treated coupled equations

## Installation
You can get the project source files under the following URl : 
https://couttet@bitbucket.org/couttet/snow_sp4e.git

old one :
ssh://git@c4science.ch/diffusion/2016/minimal_snow_model.git

## Tests
Validation tests can be run setting: `validation = True` in the main.py.
They solve the heat equation using constant boundary conditions in both numerical and analytical ways.
You can specify at which depth in the snowpack you want to compute the temperature time serie by setting: `valid_depth` to the value you want in the main.py.

## Documentation 
The documentation can be generated using Doxygen.
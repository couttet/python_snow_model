#!/usr/bin/env python
# -*- coding: utf-8 -*-

" Module containing class System"

##########################################################################
import numpy as np
import math
import sys
##########################################################################


class ValidationTest(object):

    """
class ValidationTest: to validate the program results using analytical solution
    """

    # ------------------------------------------------------------------ #
    # Constructors/Destructors                                           #
    # ------------------------------------------------------------------ #

    def __init__(self, my_system, my_parametrization):
        """__init__: 2 members representing the 2 major features solved by the program,
        initialisation by their initial value specified at the creation of the class object """

        # Members ---------------------- #
        self.system = my_system # System object
        self.parametrization = my_parametrization # parametrization object
        self.vect_T_an = []
        pass

    # ------------------------------------------------------------------ #
    # Methods                                                            #
    # ------------------------------------------------------------------ #

    # public:

    def compute_analytical_solution(self, time, constant_BC, valid_depth):
        """compute_analytical_solution: at x = snow_depth/2
        """
        self.system.compute_boundary_condition(time, 'temperature', constant_BC)
        alpha = self.parametrization.k_eff / self.parametrization.rhoCp
        sum_serie = sum((1-(-1)**n)/n * math.exp(-alpha*n*math.pi/self.system.snow_depth**2*time) * math.sin(n*math.pi/self.system.snow_depth*valid_depth) for n in range(1, 1000))
        T_an = self.system.T_bot + 2.0 * (self.system.T0 - self.system.T_bot)/math.pi * sum_serie 
        self.vect_T_an.append(T_an)
        pass

    def compare_analytical_and_numerical_solutions(self, T_num):
        """ compare analytical and numerical_solutions and return an error if the temperature divergence is too important
         """
        diff = math.fabs(np.mean(self.vect_T_an) - np.mean(T_num))
        if diff>30:
            print "Error: Program dumped, the numerical solution is too far from the analytical one !"
            sys.exit()
        pass

    



##########################################################################


if __name__ == '__main__':
    test = System()

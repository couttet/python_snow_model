#!/usr/bin/env python
# -*- coding: utf-8 -*-

" Module computing an solving linear system "

##########################################################################
import scipy.sparse as sparse
import numpy as np
from scipy import linalg
##########################################################################

class Compute(object):

    """
class Compute: Mother computation class, implementing and solving system of equation
    """

    # ------------------------------------------------------------------ #
    # Constructors/Destructors                                           #
    # ------------------------------------------------------------------ #

    def __init__(self, my_system):
        """__init__: initialize class members, vector and sparse matrix """

        # Members ---------------------- #
        self.system = my_system # system object
        self.dz = float(self.system.snow_depth)/float((self.system.N+1)) # unit space discretization
        self.A = None
        self.b = None
        pass

    def compute_right_hand_side_vector(self, top_BC, bot_BC, old_variable):
        """Compute the right hand side vector taking into account of the BC"""
        N = self.system.N
        self.b = np.zeros((N,1))
        self.b[0] = bot_BC
        self.b[N-1] = top_BC
        self.b[1:N-1] = old_variable[1:N-1]
        return self.b

    def compute_sparse_matrix(self, s):
        """Create the sparse matrix D2 for the central difference scheme approximation"""
        N = self.system.N
        self.A = np.zeros((N, N))
        self.A[0][0] = 1
        self.A[N-1][N-1] = 1
        for i in range(1,N-1):
            self.A[i][i-1] = -s[i]
            self.A[i][i] = (1+2*s[i])
            self.A[i][i+1] = -s[i]
        return self.A

    def solve_linear_system(self, A, b):
        """solve_linear_system: solve linear system of equations"""
        return linalg.solve(A,b)

    def compute(self, s, top_BC, bot_BC, old_variable):
        """Compute: compute linear system of equation and call the function to solve it """
        b = self.compute_right_hand_side_vector(top_BC, bot_BC, old_variable) # second derivative matrix
        A = self.compute_sparse_matrix(s) # explicit time integration scheme
        return self.solve_linear_system(A, b) # solve linear system


##########################################################################


if __name__ == '__main__':
    test = Compute()

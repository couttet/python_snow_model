
#!/usr/bin/env python
# -*- coding: utf-8 -*-

" Main : where the input of the program are initialized and where the program is launched "

##########################################################################
import numpy as np
import sys
##########################################################################

##########################################################################
from system import System
from parametrization import Parametrization
from compute_factory import ComputeFactory
from visualize import Visualize
##########################################################################

argc = len(sys.argv)
if (argc<2):
    print "Error: Not anough arguments provided"
    print "Usage: run " + sys.argv[0] + " number_of_nodes"
    sys.exit(-1)

""" Specifiy model numerics """
N = int(sys.argv[1]) # number of nodes
time_step = 6*3600  # = 0.25 days in [s] 4*3600
end_time = 5 * 4 * 365 * time_step # = 5 years in [s]

""" Specify model physics """
snow_depth = 10
density_snow = 300.0 # kg/m3

""" Specify vizualisation preferences """
do_print = True

""" TEMPERATURE """
T_bot = 273 # Bottom boundary conditions
temperature0 = 243.0 # Initial Conditions

""" VAPOR DENSITY"""
vapor_density0 = 0.011 # Initial Conditions

""" Create objects """
my_parametrization = Parametrization(density_snow) # Create parametrization object
my_system = System(snow_depth, N, T_bot, temperature0, vapor_density0, my_parametrization) # Create system object

""" run the programm """
my_compute_factory = ComputeFactory(my_system, my_parametrization, time_step, end_time)

#With or without validation and if so at wich depth 
validation = False
valid_depth = snow_depth/float(2) # half of the snow depth

my_compute_factory.compute_list(validation, valid_depth) # Run the program

print "The program run successfully ! "

if(do_print == True):
	# Create vizualisation object and plot
	my_visualization = Visualize(my_system, my_parametrization, my_compute_factory)
	my_visualization.plot_timeseries(validation)
	my_visualization.plot_profiles()

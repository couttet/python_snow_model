#!/usr/bin/env python
# -*- coding: utf-8 -*-

" Module computing and solving temperature PDE"

##########################################################################
from compute import Compute
##########################################################################

##########################################################################
import numpy as np
##########################################################################


class ComputeTemperature(Compute):

    """
class ComputeTemperature: Compute temperature boundary condition, PDE coefficients and solves the corresponding linear system
    """

    # ------------------------------------------------------------------ #
    # Constructors/Destructors                                           #
    # ------------------------------------------------------------------ #

    def __init__(self, my_system, my_parametrization):
        Compute.__init__(self, my_system)
        """__init__: Initialize class members """

        # Members ---------------------- #
        self.parametrization = my_parametrization # Parametrization object
        self.system = my_system # System object
        pass

    # ------------------------------------------------------------------ #
    # Methods                                                            #
    # ------------------------------------------------------------------ #

    # public:
    def compute_coefficient(self, dt):
        """compute_coefficient: compute coefficient for the finite difference method
        @param dt: time step
        """
        return (self.parametrization.k_eff * np.ones((self.system.N,1)) / self.parametrization.rhoCp) * float(dt)/(float(self.dz)**2)

    def temperature_pde(self, time, dt, Told, constant_BC):
        """temperature_pde: compute boundary condition, compute PDE and solve corresponding linear system
        @param time
        @param dt
        @param Told
        """
        self.system.compute_boundary_condition(time, 'temperature', constant_BC)
        return self.compute(self.compute_coefficient(dt), self.system.T_top, self.system.T_bot, Told)

##########################################################################


if __name__ == '__main__':
    test = ComputeTemperature()

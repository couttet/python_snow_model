#!/usr/bin/env python
# -*- coding: utf-8 -*-

" Module computing and solving vapor density PDE"

##########################################################################
from compute import Compute
##########################################################################

##########################################################################
import numpy as np
##########################################################################


class ComputeVaporDensity(Compute):

    """
class ComputeVaporDensity: Compute water vaèpor boundary condition, PDE coefficients and solves the corresponding linear system
    """

    # ------------------------------------------------------------------ #
    # Constructors/Destructors                                           #
    # ------------------------------------------------------------------ #

    def __init__(self, my_system, my_parametrization, my_compute_temp):
        Compute.__init__(self, my_system)
        """__init__: initialize class members """

        # Members ---------------------- #
        self.system = my_system
        self.parametrization = my_parametrization # parametrization object
        self.compute_T = my_compute_temp #compute temperature object
        pass

    # ------------------------------------------------------------------ #
    # Methods                                                            #
    # ------------------------------------------------------------------ #

    def compute_coefficient(self, dt):
        """compute_coefficient: compute coefficient for the finite difference method
        @param dt: time step
        """
        return self.parametrization.D_eff * np.ones((self.system.N,1)) / (1 - self.parametrization.ice_volume_fraction) * (float(dt)/(float(self.dz)**2))

    def vapor_density_pde(self, time,  dt, V_old, constant_BC):
        """vapor_density_pde: compute boundary condition, compute PDE and solve corresponding linear system
        @param time
        @param dt
        @param Vold
        """
        self.system.compute_boundary_condition(time, 'vapor', constant_BC)
        return self.compute(self.compute_coefficient(dt), self.system.V_top, self.system.V_bot, V_old)


##########################################################################

if __name__ == '__main__':
    test = ComputeTemperature()

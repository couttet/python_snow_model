#!/usr/bin/env python
# -*- coding: utf-8 -*-

" Module containing class System"

##########################################################################
import numpy as np
import math
##########################################################################


class System(object):

    """
class System: contains the 2 major features solved by the program, 
-snowpack temperature profile-
-vapor density
    """

    # ------------------------------------------------------------------ #
    # Constructors/Destructors                                           #
    # ------------------------------------------------------------------ #

    def __init__(self, snow_depth, N , T_bot, temperature0, vapor_density0, my_parametrization):
        """__init__: 2 members representing the 2 major features solved by the program,
        initialisation by their initial value specified at the creation of the class object """

        # Members ---------------------- #
        self.snow_depth = snow_depth # Snowpack depth
        self.N = N # number of nodes
        self.T_top = None # upper temperature boundary condition
        self.T_bot = T_bot # constant bottom temperature boundary condition
        self.vect_T_top = [] # Vector storing top boundary temperature evolution with time   
        self.delta_T_0 = 20 # Initial temperature difference between top and bottom snowpack (K)
        self.V_top = None
        self.V_bot = None
        self.vect_V_top = []
        self.parametrization = my_parametrization # parametrization object
        self.T0 = temperature0
        self.temperature =  temperature0*np.ones((self.N,1)) # Snowpack temperature (K)
        self.vapor_density = vapor_density0*np.ones((self.N,1)) # Snowpack vapor density (kg/m3)
        pass

    # ------------------------------------------------------------------ #
    # Methods                                                            #
    # ------------------------------------------------------------------ #

    # public:

    def set_variable(self, variable_name ,variable_up):
        """_set_variable: assign the specified value to a specific members.
        @param variable_name: name of the targeted variable
        @param variable_up: value assigned to the targeted variable
        """
        if (variable_name == 'temperature'):
            self.temperature = variable_up
        elif (variable_name == 'vapor_density'):
            self.vapor_density = variable_up
        pass

    def get_variable(self, variable_name):
        """_get_variable: return the specified member's value.
        @param variable_name: name of the variable to retrieve the value from
         """
        if (variable_name == 'temperature'):
            return self.temperature
        elif (variable_name == 'vapor_density'):
            return self.vapor_density

    def compute_boundary_condition(self, time, variable_name, constant_BC):
        """compute_boundary_condition: compute surface temperature as a boundary condition using omega to mimic seasonal temperature signal on the surface
        @param time: simulation time evolution
        """
        if (variable_name == 'temperature'):
            if constant_BC == True: #set constant upper temperature for comparison
                self.T_top = 273.0
            elif constant_BC == False:
                omega = 2 * math.pi / (24 * 365 * 3600) # (s-1)
                self.T_top = self.T_bot + self.delta_T_0 * math.sin(omega * time) # temperature oscillation (K)
            self.vect_T_top.append(self.T_top) # store top boundary temperature values in a vector
        elif (variable_name == 'vapor'):
            self.V_top = self.parametrization.compute_equilibrium_vapor_density(self.T_top)
            self.vect_V_top.append(self.V_top)
            self.V_bot = self.parametrization.compute_equilibrium_vapor_density(self.T_bot)
        pass


##########################################################################

if __name__ == '__main__':
    test = System()

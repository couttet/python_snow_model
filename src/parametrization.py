#!/usr/bin/env python
# -*- coding: utf-8 -*-

" Module containing the program parametrization"

##########################################################################
import numpy as np
##########################################################################


class Parametrization(object):

    """
class Parametrization: define the parametrized parts of the system, necessary for simplication purposes.
    """

    # ------------------------------------------------------------------ #
    # Constructors/Destructors                                           #
    # ------------------------------------------------------------------ #

    def __init__(self, density_snow):
        """__init__: initialize some constant numerical values as well as the initialized system """

        # Members ---------------------- #
        self.rhoCp = 5.3e5 # Transient term coefficient: snow density times specific heat capacity (J/m3/K)
        self.density_snow = density_snow # Snow density (kg/m3)
        self.density_ice = 917.0 # Ice density (kg/m3)
        self.ice_volume_fraction = self.density_snow / self.density_ice # Ice volume fraction (m3/m3) approximation valable in case of dry snow cover
        self.k_eff = self.compute_effective_thermal_conductivity() # Snow thermal conductivity (W/mK)
        self.D_eff = self.compute_effective_vapor_diffusivity() # Snow effective vapor diffusivity
        pass

    # ------------------------------------------------------------------ #
    # Methods                                                            #
    # ------------------------------------------------------------------ #

    # public:

    def compute_equilibrium_vapor_density(self, temperature):
        """ equilibrium vapor_density: from Libbrecht """
        T_celsius = temperature - 273.15 # (°C) 
        p_eq = (3.6646e10 - 1.3086e6*T_celsius - 33793*np.power(T_celsius,2)) * np.exp(-6150/temperature) # (mbar)
        p_eq *= 100 # (Pa)
        spe_gas_cte = 461.401 # specific gas constant for water vapor (J/(kg K))
        return p_eq / spe_gas_cte / temperature # water vapor concentration (from ideal gas law)

    def compute_effective_thermal_conductivity(self):
        """ effective_thermal_conductivity:  empirical curve fitting from Calonne,2011 """
        return 2.5e-6 * self.density_snow**2 - 1.23e-4 * self.density_snow + 0.024 # (J/(Kms))

    def compute_effective_vapor_diffusivity(self):
        """ effective_vapour_diffusivity:  empirical curve fitting from Calonne """
        D_0 = 2.0e-5 #TODO
        return D_0 * (3.0 * (1 - self.ice_volume_fraction) -1) / 2 #m/s


##########################################################################


if __name__ == '__main__':
    test = System()

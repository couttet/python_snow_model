#!/usr/bin/env python
# -*- coding: utf-8 -*-

" Module listing and organizing the call of compute methods "

##########################################################################
from compute_temperature import ComputeTemperature
from compute_vapor_density import ComputeVaporDensity
from validation_test import ValidationTest
import numpy as np
import scipy.sparse as sparse
import csv
##########################################################################


class ComputeFactory(object):

    """
class ComputeFactory: list the order with which the different calculation should be done
    """

    # ------------------------------------------------------------------ #
    # Constructors/Destructors                                           #
    # ------------------------------------------------------------------ #

    def __init__(self, my_system, my_parametrization, time_step, t_end):
        """__init__: Initialize numerical parameters and model physics required to solve the problem """

        # Members ---------------------- #
        self.dt = time_step
        self.t_0 = 0 # start time
        self.t_end = t_end # end time
        self.system = my_system # system object
        self.compute_T = ComputeTemperature(my_system, my_parametrization) # Create compute temperature
        self.compute_V = ComputeVaporDensity(my_system, my_parametrization, self.compute_T) # Create compute vapor
        self.validation_test = ValidationTest(my_system, my_parametrization)
        self.T_old = None 
        self.T_guess = None
        self.V_old = None
        self.V_guess = None
        self.vect_T_num = []
        pass

    def compute_list(self, validation, valid_depth):
        """compute_list: algorithm to solve set of equations"""
        loop = 0
        print "Code is running..."
        print "tend = ", self.t_end, " seconds "
        print "time step = ", self.dt, "seconds "
        print "number of time steps = ", (self.t_end/self.dt)
        for t in range(self.t_0, self.t_end, self.dt):

            self.T_old = self.system.get_variable('temperature')
            if validation == True:
                """ TEMPERATURE """
                self.validation_test.compute_analytical_solution(t, validation, valid_depth)
                self.T_guess = self.compute_T.temperature_pde(t, self.dt, self.T_old, validation)
                self.system.set_variable('temperature', self.T_guess)
                z_index = int((self.system.N+1)*valid_depth/self.system.snow_depth)
                self.vect_T_num.append(self.T_guess[z_index])
                self.validation_test.compare_analytical_and_numerical_solutions(self.T_guess)
                """ VAPOR DENSITY """
                self.V_old = self.system.get_variable('vapor_density')
                self.V_guess = self.compute_V.vapor_density_pde(t, self.dt, self.V_old, validation)
                self.system.set_variable('vapor_density', self.V_guess)
            elif validation == False:
                """ TEMPERATURE """
                self.T_old = self.system.get_variable('temperature')
                self.T_guess = self.compute_T.temperature_pde(t, self.dt, self.T_old, validation)
                self.system.set_variable('temperature', self.T_guess)
            """ VAPOR DENSITY """
            self.V_old = self.system.get_variable('vapor_density')
            self.V_guess = self.compute_V.vapor_density_pde(t, self.dt, self.V_old, validation)
            self.system.set_variable('vapor_density', self.V_guess)
            loop +=1

        return [self.T_guess, self.V_guess]



##########################################################################


if __name__ == '__main__':
    test = Compute()

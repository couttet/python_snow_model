#!/usr/bin/env python
# -*- coding: utf-8 -*-

" Module containing visualization of the system  "
##########################################################################
import matplotlib.pyplot as plt
##########################################################################

class Visualize:
	
    def __init__(self, my_system, my_parametrization, my_compute_factory):
        """__init__: Initialize class members """
        self.system = my_system
    	self.parametrization = my_parametrization
        self.factory = my_compute_factory
    	pass

    def plot_timeseries(self, validation):
        """plot_parameters: plot upper boundary conditions for temperature and vapor density"""
        if validation == True:
            f, (ax1, ax2, ax3) = plt.subplots(3, 1) #, sharey=True
            ax3.plot(self.factory.validation_test.vect_T_an, 'k-', label = 'Analytical solution at z=L/2')
            ax3.plot(self.factory.vect_T_num, 'r-', label = 'Numerical solution at z=L/2')
            ax3.set_xlabel('Number of timesteps', fontsize=14)
            ax3.set_ylabel('Temperature (K)',fontsize=14)
            ax3.legend(loc=4)
        else:
            f, (ax1, ax2) = plt.subplots(2, 1) #, sharey=True
        ax1.plot(self.factory.system.vect_V_top, 'g')
        ax1.set_xlabel('Number of timesteps', fontsize=14)
        ax1.set_ylabel('Equ vapor density (kg/m3)', fontsize=14)

        ax2.plot(self.factory.system.vect_T_top, 'k')
        ax2.set_xlabel('Number of timesteps', fontsize=14)
        ax2.set_ylabel('Surface temperature (K)',fontsize=14)
        plt.show()
        pass

    def plot_profiles(self):
        """plot_variables: plot results of the PDE solving """
        f, (ax1, ax2) = plt.subplots(2, 1) #, sharey=True
        ax1.plot(self.system.temperature, 'b-o')
        ax1.set_xlabel('Nodes',fontsize=14)
        ax1.set_ylabel('Temperature profile (K)',fontsize=14)

		# Instead of displaying the vapor density it is more instructive to display the supersaturation
        supersaturation = (self.system.vapor_density / self.parametrization.compute_equilibrium_vapor_density(self.system.temperature) -1)
        ax2.plot(supersaturation, 'r-o')
        ax2.set_xlabel('Nodes',fontsize=14)
        ax2.set_ylabel('Supersaturation (-)',fontsize=14)
        plt.show()
        pass
